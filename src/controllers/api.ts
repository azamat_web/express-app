"use strict";

import axios from "axios";
import request from "request";
import graph from "fbgraph";
import { Response, Request, NextFunction } from "express";
import { AuthToken } from "../../src/models/User";


/**
 * GET /api
 * List of API examples.
 */
export let getApi = (req: Request, res: Response) => {
  res.render("api/index", {
    title: "API Examples"
  });
};

/**
 * friend list VK middleware
 */
export let getFriends = async (req: Request, res: Response, next: NextFunction) => {
  const token = req.user.tokens.find((token: AuthToken) => token.kind === "vkontakte");
  const fields = "nickname,photo_50";
  const count = 5;
  const result = await axios.get(`https://api.vk.com/method/friends.get?fields=${fields}&count=${count}&access_token=${token.accessToken}&v=5.52`);
  req.session.friends = result.data.response.items;
  return next();
};